# Argo Workflow Development

GFANZ is working towards an automated, reproducible, bioinformatics analysis cloud computing system. The aim is to improve reliability, cost effectiveness, and democratise bioinformatics. This part of the project brings it all together. Here we develop and test Argo workflows for use with GFANZ docker containers and kubernetes in an openstack cloud environment.

# Project Information

We provide instructions on getting Argo and prerequisites running on an OpenStack environment. As the project progresses, we will be developing workflow files (yaml) for a variety of bioinformatics workflows.

[Here](https://gitlab.com/gfanz/argo-workflow-development/-/wikis/home) is our documentation.

# Argo Docs

https://argoproj.github.io/docs/argo/examples/readme.html

# Argo Youtube presentation

https://www.youtube.com/watch?v=qq6eEC8ME_k