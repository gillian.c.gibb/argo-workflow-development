# Argo Controller Jump Server

The install.sh script installs the required components to set up and control an Argo instance. It is intended to be used inside an openstack cloud environment. The user would ssh into the jump server and control Argo from inside. 

In addition to the software the script installs, it is necessary to have an appropriate OpenStackRC file and source it. See this [documentation](https://docs.catalystcloud.nz/sdks-and-toolkits/cli/configure.html#source-rc-file) for instructions using CatalystCloud.